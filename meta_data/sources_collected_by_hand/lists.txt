# Security
https://urlhaus.abuse.ch/downloads/hostfile/ blocklist_abuse_ch_urlhaus.domains
https://dsi.ut-capitole.fr/blacklists/download/phishing.tar.gz blocklist_ut_capitole_phishing_malware.domains
https://raw.githubusercontent.com/durablenapkin/scamblocklist/master/hosts.txt blocklist_durablenapkin_scamblocklist.domains
https://osint.digitalside.it/Threat-Intel/lists/latestdomains.txt blocklist_digitalside_threat_intel.domains
https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/Mandiant_APT1_Report_Appendix_D.txt blocklist_ethanr_mandiant.domains
https://bitbucket.org/ethanr/dns-blacklists/raw/8575c9f96e5b4a1308f2f12394abd86d0927a4a0/bad_lists/hosts.txt blocklist_ethanr_hosts.domains
https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-malware.txt blocklist_quidsup_no_track_malware.domains
https://raw.githubusercontent.com/Spam404/lists/master/main-blacklist.txt blocklist_spam404_main_blacklist.domains
https://raw.githubusercontent.com/HorusTeknoloji/TR-PhishingList/master/url-lists.txt blocklist_horus_teknoloji_phishing.domains
https://blocklistproject.github.io/Lists/alt-version/abuse-nl.txt blocklist_blocklistproject_abuse.domains
https://blocklistproject.github.io/Lists/alt-version/fraud-nl.txt blocklist_blocklistproject_fraud.domains
https://blocklistproject.github.io/Lists/alt-version/malware-nl.txt blocklist_blocklistproject_malware.domains
https://blocklistproject.github.io/Lists/alt-version/phishing-nl.txt blocklist_blocklistproject_phishing.domains
https://blocklistproject.github.io/Lists/alt-version/ransomware-nl.txt blocklist_blocklistproject_ransomware.domains
https://blocklistproject.github.io/Lists/alt-version/scam-nl.txt blocklist_blocklistproject_scam.domains
https://raw.githubusercontent.com/deathbybandaid/piholeparser/master/Subscribable-Lists/ParsedBlacklists/Disconnect-Malware-Filter.txt blocklist_deathbybandaid_disconnect_malware.domains
https://raw.githubusercontent.com/deathbybandaid/piholeparser/master/Subscribable-Lists/ParsedBlacklists/Disconnect-Malvertising-Filter.txt blocklist_deathbybandaid_disconnect_malvertising.domains
https://raw.githubusercontent.com/PolishFiltersTeam/KADhosts/master/KADhosts.txt blocklist_polishfiltersteam_kadhosts.domains
https://dsi.ut-capitole.fr/blacklists/download/ddos.tar.gz blocklist_ut_capitole_ddos.domains

# Privacy
https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts blocklist_stevenblack_hosts.domains
https://v.firebog.net/hosts/Easyprivacy.txt blocklist_firebog_easy_privacy.domains
https://gitlab.com/quidsup/notrack-blocklists/raw/master/notrack-blocklist.txt blocklist_quidsup_no_track.domains
https://raw.githubusercontent.com/Perflyst/PiHoleBlocklist/master/android-tracking.txt blocklist_perflyst_android_tracking.domains
https://v.firebog.net/hosts/AdguardDNS.txt blocklist_firebog_adguard_dns.domains
https://raw.githubusercontent.com/anudeepND/blacklist/master/adservers.txt blocklist_anudeep_adservers.domains
https://raw.githubusercontent.com/RooneyMcNibNug/pihole-stuff/master/SNAFU.txt blocklist_rooneymcnibnug_snafu.domains
https://hostsfile.mine.nu/hosts0.txt blocklist_hostsfile_mine_hosts0.domains
#https://adblock.mahakala.is blocklist_mahakala_adblock.domains
#https://adaway.org/hosts.txt blocklist_adaway_hosts.domains
https://v.firebog.net/hosts/Easylist.txt blocklist_firebog_easylist.domains
https://raw.githubusercontent.com/FadeMind/hosts.extras/master/add.2o7Net/hosts blocklist_fademind_no_track.domains
https://hostfiles.frogeye.fr/firstparty-trackers-hosts.txt blocklist_frogeye_firstparty_trackers.domains
https://blocklistproject.github.io/Lists/alt-version/ads-nl.txt blocklist_blocklistproject_ads.domains
https://blocklistproject.github.io/Lists/alt-version/tracking-nl.txt blocklist_blocklistproject_tracking.domains
https://block.energized.pro/basic/formats/domains.txt blocklist_energized_protection_basic.domains
https://raw.githubusercontent.com/bigdargon/hostsVN/master/hosts blocklist_bigdargon_hostsVN.domains
https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/spy.txt blocklist_crazymax_windows_spy.domains
https://blocklistproject.github.io/Lists/alt-version/smart-tv-nl.txt blocklist_blocklistproject_smart_tv.domains
https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Hosts/GoodbyeAds.txt blocklist_goodbyads_default.domains
https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Extension/GoodbyeAds-Xiaomi-Extension.txt blocklist_goodbyads_xiamo.domains
https://raw.githubusercontent.com/jerryn70/GoodbyeAds/master/Extension/GoodbyeAds-Samsung-AdBlock.txt blocklist_goodbyads_samsung.domains
https://dsi.ut-capitole.fr/blacklists/download/publicite.tar.gz blocklist_ut_capitole_publicite.domains

# Google
https://raw.githubusercontent.com/nickspaargaren/no-google/master/pihole-google.txt blocklist_nickspaargaren_google.domains

# Facebook
https://blocklistproject.github.io/Lists/alt-version/facebook-nl.txt blocklist_blocklistproject_facebook.domains
https://blocklistproject.github.io/Lists/alt-version/whatsapp-nl.txt blocklist_blocklistproject_whatsapp.domains

# TikTok
https://blocklistproject.github.io/Lists/alt-version/tiktok-nl.txt blocklist_blocklistproject_tiktok.domains

# Microsoft
https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/extra.txt blocklist_crazymax_windows_extra.domains
https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/update.txt blocklist_crazymax_windows_update.domains

# Crypto
https://blocklistproject.github.io/Lists/alt-version/crypto-nl.txt blocklist_blocklistproject_crypto.domains
https://dsi.ut-capitole.fr/blacklists/download/bitcoin.tar.gz blocklist_ut_capitole_bitcoin.domains
https://dsi.ut-capitole.fr/blacklists/download/cryptojacking.tar.gz blocklist_ut_capitole_cryptojacking.domains

# Porn
https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/pornography-hosts blocklist_sinfonietta_pornography-hosts.domains
https://raw.githubusercontent.com/chadmayfield/pihole-blocklists/master/lists/pi_blocklist_porn_all.list blocklist_chadmayfield_pi_blocklist_porn_all.domains
https://block.energized.pro/porn/formats/domains.txt blocklist_energized_protection_porn.domains
https://dsi.ut-capitole.fr/blacklists/download/adult.tar.gz blocklist_ut_capitole_adult.domains
https://raw.githubusercontent.com/mhxion/pornaway/master/hosts/porn_ads.txt blocklist_pornaway_porn_ads.domains
https://raw.githubusercontent.com/mhxion/pornaway/master/hosts/porn_sites.txt blocklist_pornaway_porn_sites.domains
https://blocklistproject.github.io/Lists/alt-version/porn-nl.txt blocklist_blocklistproject_porn.domains
