# Rules 2.0

This repository contains all scripts that we use to update our additive filter rules. Additionally, it contains our self-made domain-lists in the folder [data_raw/lists_handmade/](data_raw/lists_handmade).

Our final blocklists that we use in Gardion VPN are published in [Rules3000](https://gitlab.com/gardion-com/rules3000) and created with this repository.

## How it works / repository structure
All scripts (subroutines) are located in the [scripts/](scripts) folder. If you run [run.sh](run.sh), all necessary steps to update our rules from their sources and upload them into [Rules3000](https://gitlab.com/gardion-com/rules3000) are performed.
The script then checks whether a new version of a list is available and downloads them into [data_raw/](data_raw). If a new version was downloaded, domains will be extracted from this list by removing comments and performing a regular expression (regex) search using the domain regex from [scripts/regex.sh](scripts/regex.sh). The resulting domain lists will be placed in the folder *[data_preprocessed/](data_preprocessed)*.  

All lists that come from external sources together with their download URL can be found [here](meta_data/sources_collected_by_hand/lists.txt). The lists that are maintained by Gardion are located [here](data_raw/lists_handmade).  

Based on the preprocessed domain lists, the [group_into_categories.sh](scripts/group_into_categories.sh) script groups all lists into categories. The definition of each category is located in [meta_data/group_info/categories/](meta_data/group_info/categories). The resulting domain lists for each category will be placed in *[data_in_categories/](data_in_categories)*.

As our lists may contain some false-positives, we include adjustments in each category. The adjustments are located in [adjustments_for_categories/](adjustments_for_categories) and are applied by running [adjust_filter_sets.sh](scripts/adjust_filter_sets.sh). We distinguish between general and category specific block- and passlists. The general adjustments are applied to all categories and the specific ones only to the specific category. The additional domains from the adjustment blocklist will be added to the block list for each category and afterwards we remove all domains from the passlists. Thus, passlist domains can overwrite blocklist domains. The resulting domain lists will be located in *[data_in_categories_adjusted/](data_in_categories_adjusted)*.

Finally, we transform our adjusted domain lists into lua modules using [transform_to_lua.sh](scripts/transform_to_lua.sh). The resulting block lists are stored in *[rules3000/](rules300)* and uploaded to the [Rules3000](https://gitlab.com/gardion-com/rules3000) repository.


**Annotation:** All folder and file links that are italic like *folder/* are not included in this repository at the moment. But they will be created locally if you run our scripts.
