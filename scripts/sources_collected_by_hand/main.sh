#!/bin/bash
# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]
then
  root_dir=../..
fi
# Update list files
source $root_dir/scripts/sources_collected_by_hand/update_lists.sh