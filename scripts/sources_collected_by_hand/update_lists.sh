#!/bin/bash

# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=../..
fi
# Set up location for the lists.txt. It contains the URL together with a supposed filename
lists_file=$root_dir/meta_data/sources_collected_by_hand/lists.txt
# Set up locations for relevant scripts
download_script=$root_dir/scripts/download.sh
handle_compression_script=$root_dir/scripts/handle_compression.sh
regex_script=$root_dir/scripts/regex.sh
# Set up location for the download directories
download_location=$root_dir/data_preprocessed/sources_collected_by_hand/
download_location_raw=$root_dir/data_raw/sources_collected_by_hand/
mkdir -p $download_location     # Directory for parsed lists (contains only IPs and Domains)
mkdir -p $download_location_raw # Directory for downloaded files
# Export variables to use them with parallel function calls
export download_script
export handle_compression_script
export regex_script
export download_location
export download_location_raw

# Define a function that processes one line from $lists_file
update_list() {
  # Skip empty lines and comments, i.e. a line which starts with a "#","!" or "//".
  case "$1" in
    ""|"#"*|"!"*|"//"*)  exit
  esac
  # Split line into url and filename
  count=0
  for part in $1; do
    if [ $count -eq 0 ]
    then
      url="$part"
    else
      name="$part"
    fi
    ((count++))
  done
  #echo URL: "$url"
  #echo Name: "$name"
  # The value successful_download indicates whether a download, took place or not. It remains with 0 if the file was
  # not modified
  successful_download=0
  destination_raw="$download_location_raw$name"
  destination="$download_location$name"
  output_download_script=$(/bin/bash "$download_script" "$url" "$destination_raw" 2>&1)
  exit_code=$?
  echo "$output_download_script"
  if [ $exit_code -eq 0 ]; then
    # Download scripts exits with code 0 if either a download was successful or the file was not modified
    # But only if file was uploaded again the domain/ip extraction should be applied to the downloaded content
    if [[ "$output_download_script" != *"304: Not Modified."* ]]; then
      successful_download=1
    fi
  else
    # Download was not successful
    # Remove previously created file to avoid any conflict with the download script.
    rm "$destination_raw"
  fi
  if [ $successful_download -eq 1 ]; then
    # Handle file to uncompress it or remove it if the type is incompatible like e.g. a html file
    source "$handle_compression_script" "$destination_raw" "$destination"
    # If the destination file was deleted by the handle_compression_script, do not try to extract domains.
    if [ -f "$destination" ]; then
      # Load regular expressions from file
      source "$regex_script"
      # Introduce a temporary file
      temp_file_name=temp_file_$$
      # Extract all domains from the file and assume comments to start with #, ! or //
      grep -E -o "^[^#\!]*" "$destination" |
      grep -E -o "^([^\/]|\/[^\/])*" |
      grep -E -o "$regex_domain" |
      grep -E --invert-match "$regex_exclude_localhosts" \
        >"$temp_file_name"
      mv "$temp_file_name" "$destination"
    fi
  fi
}
export -f update_list

# Update lists in parallel
parallel update_list <"$lists_file"