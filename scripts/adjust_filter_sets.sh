#!/bin/bash
# This script involves the adjustments into the merged filter sets.

# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=..
fi

# Set up variables for locations. Export them to use them inside a parallel function call.
adjustments_folder=$root_dir/adjustments_for_categories/
filter_set_location=$root_dir/data_in_categories/
final_location=$root_dir/data_in_categories_adjusted/
regex_script=$root_dir/scripts/regex.sh
mkdir -p $final_location
export adjustments_folder
export filter_set_location
export final_location
export regex_script

# Define a function that processes one filter set. It performs all relevant adjustments from $adjustments_folder.
# Note that all domains from a filter list should be blocked.
# The merged filter set's filename must be given as $1. It is located inside $filter_set_location
process_filter_set_domains() {
  # Path to filter set file
  filter_set_filename="$1"
  # Name of the filter set, i.e. filename without its suffix .domains
  filter_set_name="${1%.domains}"
  # Path to the output file
  final_destination=$final_location$filter_set_name.domains
  # Path to general and specific adjustment files
  general_blocklist_path=${adjustments_folder}general_blocklist.domains
  general_passlist_path=${adjustments_folder}general_passlist.domains
  specific_blocklist_path="${adjustments_folder}${filter_set_name}"_blocklist.domains
  specific_passlist_path="${adjustments_folder}${filter_set_name}"_passlist.domains
  # temp filenames that are used during the function. Data is moved to final_destination later.
  temp_file_1="${final_location}temp_file_$$"
  temp_file_2="${final_location}temp_file_${filter_set_name}_$$_$$"
  cat "$filter_set_location$filter_set_filename" >>"$temp_file_1"
  # Load regular expressions from file
  source "$regex_script"
  # Define a list which holds all entries that should be added to the blocklist and a list with entries that should be
  # removed.
  add_to_filter_set=()
  remove_from_filter_set=()
  # Collect domain entries from general block list adjustment if file exists.
  if [ -f "$general_blocklist_path" ]; then
    add_to_filter_set+=($(grep -E -o "^[^#\!]*" "$general_blocklist_path" |
                            grep -E -o "^([^\/]|\/[^\/])*" |
                            grep -E -o "$regex_domain"))
  fi
  # Collect domain entries from specific block list adjustment if file exists.
  if [ -f "$specific_blocklist_path" ]; then
    add_to_filter_set+=($(grep -E -o "^[^#\!]*" "$specific_blocklist_path" |
                            grep -E -o "^([^\/]|\/[^\/])*" |
                            grep -E -o "$regex_domain"))
  fi
  # Collect entries from general passlist if file exists.
  if [ -f "$general_passlist_path" ]; then
    remove_from_filter_set+=($(grep -E -o "^[^#\!]*" "$general_passlist_path" |
                                grep -E -o "^([^\/]|\/[^\/])*" |
                                grep -E -o "$regex_domain"))
  fi
  # Collect entries from specific passlist if file exists.
  if [ -f "$specific_passlist_path" ]; then
    remove_from_filter_set+=($(grep -E -o "^[^#\!]*" "$specific_passlist_path" |
                                grep -E -o "^([^\/]|\/[^\/])*" |
                                grep -E -o "$regex_domain"))
  fi

  # Add blocklist entries to temp_file_1 if there are any and perform deduplication
  if [ ${#add_to_filter_set[@]} -gt 0 ]; then
    echo "${add_to_filter_set[@]}" | tr " " "\n" >>"$temp_file_1"
    sort -u "$temp_file_1" >"$temp_file_2"
    mv -f "$temp_file_2" "$temp_file_1"
  fi
  # Remove passlist entries from temp_file_1 if there are any
  if [ ${#remove_from_filter_set[@]} -gt 0 ]; then
    comm -23 "$temp_file_1" <(echo "${remove_from_filter_set[@]}" | tr " " "\n" | sort) >"$temp_file_2"
    mv -f "$temp_file_2" "$temp_file_1"
  fi

  # Copy rules into temp_file_2
  while read domain; do
    echo "$domain" >> "$temp_file_2"
  done <"$temp_file_1"
  #echo local-zone: asdf  \"example.com\" always_eioslkdfj>>"$temp_file_2"

  # Delete temp files and move $temp_file_2 into $final_destination
  rm "$temp_file_1"
  mv "$temp_file_2" "$final_destination"
  echo Filter set "$filter_set_name" adjusted

}
export -f process_filter_set_domains

# Process all filter sets in parallel using all .domains files from $filter_set_location
parallel process_filter_set_domains ::: "$(ls -p $filter_set_location | grep -E "*.domains$")"
