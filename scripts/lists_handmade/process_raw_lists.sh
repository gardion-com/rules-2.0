#!/bin/bash
# This script processes all handmade lists, i.e. removes comments and extract domains

# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=../..
fi
# Set up location variables for the folders containing the lists or where they will be placed after processing.
location_lists_raw=$root_dir/data_raw/lists_handmade/       # Directory for raw lists
location_lists=$root_dir/data_preprocessed/lists_handmade/  # Directory for parsed lists (contains only IPs and Domains)
mkdir -p $location_lists
# Set up locations for relevant scripts
regex_script=$root_dir/scripts/regex.sh
# Export variables to use them with parallel function calls
export regex_script
export location_lists
export location_lists_raw

# Define a function that processes one file. It assumes the filename as an argument.
process_file() {
  filename="$1"
  # Change filename ending such that it always ends with .domains
  case "$filename" in
  *".domains") new_filename=$filename ;;
  *".rules") new_filename=${filename%.rules}.domains ;;
  *) new_filename=${filename}.domains ;;
  esac
  # Load regular expressions from file
  source "$regex_script"
  # Extract all domains from the file and assume comments to start with #, ! or //
  # For .rules file assume them in an unbound format and therefore only extract domains after "local-zone: domain"
  case "$filename" in
  *".rules") grep -E -o "^[^#\!]*" "$location_lists_raw$filename" |
              grep -E -o "^([^\/]|\/[^\/])*" |
              grep -E "local-zone:" |
              grep -E -o "$regex_domain" |
              grep -E --invert-match "$regex_exclude_localhosts" \
                >"$location_lists/$new_filename" ;;
  *) grep -E -o "^[^#\!]*" "$location_lists_raw$filename" |
      grep -E -o "^([^\/]|\/[^\/])*" |
      grep -E -o "$regex_domain" |
      grep -E --invert-match "$regex_exclude_localhosts" \
        >"$location_lists/$new_filename" ;;
  esac
}
export -f process_file

# list all files form the directory $location_lists_raw and skip folders
processable_files=$(ls -p $location_lists_raw | grep --invert-match /)
#process raw lists in parallel
parallel process_file ::: "$processable_files"
