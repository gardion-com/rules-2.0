#!/bin/bash
# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]
then
  root_dir=../..
fi
# Process handmade lists
source $root_dir/scripts/lists_handmade/process_raw_lists.sh