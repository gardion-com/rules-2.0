# This file contains regular expressions for domains, IPv4 and later also IPv6. These regular expressions can be used
# to filter items of the specific type from a filter list. IPv4 and IPv6 adresses can also conatin a netmask.

regex_domain="(([a-z0-9]+|([a-z0-9][-a-z0-9]{1,2}|xn--)[a-z0-9]([-a-z0-9]*[a-z0-9])?)\.)+(xn--[a-z0-9]+|[a-z]{2,})"
regex_ipv4="((2([0-4][0-9]|5[0-5])|[0-1]?[0-9]{1,2})\.){3}(2([0-4][0-9]|5[0-5])|[0-1]?[0-9]{1,2})(\/[0-3]?[0-9])?"

regex_exclude_localhosts="(localhost.localdomain|localhost4.localdomain4|localhost6.localdomain6)"

# Todo: Add regex for comments to this file