#!/bin/bash
# This scripts requires two optional arguments. Arg1=input_file, Arg2=output_file. Arg1 contains the path to the file
# that should be handled by this script. The processed file will be stored in tha path contained in Arg2.

# Parse arguments
input_file=$1
output_file=$2
if [ $# -lt 2 ]
 then
   echo "$0: Not enough arguments given. Two arguments required. Arg1=input_file, Arg2=output_file"
   exit 1
fi

# Define a name for a temporary used folder. The folder will be used to uncompress files to it like in the case of
# a .tar file in order to avoid name collisions.
temp_folder_name=temp_folder_$$/
# Define a filename for a temporary used file. The file will be used to extract compressed files into it.
temp_file_name=temp_file_$$

# copy input file to output file
cp "$input_file" "$output_file"

# Handle nested compression
# Todo: Check if compressed files really contain a file and not an empty folder. => Exclude folders from extraction.
while :
do
  type=$(file "$output_file" | tr "[:upper:]" "[:lower:]")  # Transform the result into lower case
  #echo "$type"
  case "$type" in
    *html*|empty) rm "$output_file"
                  break;;
    *gzip*) mv "$output_file" "$output_file".gz
            # Note that gzip-files contain only one file
            gzip -d "$output_file".gz;;
    *tar*)  # Extract archive only if it contains either one file or one file with "domains" or "hosts" in its name.
            found_suitable_file=0
            number_contained_files=$(tar -tf "$output_file" | wc -l)
            if [ "$number_contained_files" -eq 1 ]; then
              contained_filename=$(tar -tf "$output_file")
              found_suitable_file=1
            elif [ "$(tar -tf "$output_file" | grep -c domains)" -eq 1 ]; then
              # Assume a domains file inside the .tar-file
              contained_filename=$(tar -tf "$output_file" | grep domains)
              found_suitable_file=1
            elif [ "$(tar -tf "$output_file" | grep -c hosts)" -eq 1 ]; then
              # Assume a hosts file inside the .tar-file
              contained_filename=$(tar -tf "$output_file" | grep hosts)
              found_suitable_file=1
            fi
            # Extract the file only if a suitable file was found
            if [ $found_suitable_file -eq 1 ]; then
              mkdir "$temp_folder_name"
              tar -C $temp_folder_name -xf "$output_file" "$contained_filename"
              mv "$temp_folder_name$contained_filename" "$output_file"
              rm -r "$temp_folder_name"
            else
              # No suitable file was found, then discard the file.
              rm "$output_file"
              break
            fi;;
    *7-zip*)  number_contained_files=$(7zr l "$output_file" | grep -c " 1 file")
              # Extract archive only if it contains just one file.
              # Todo: Extract the relevant file, see e.g. Filterlists.com id 1994
              if [ "$number_contained_files" -eq 1 ]; then
                7zr e -so "$output_file" >"$temp_file_name"
                mv "$temp_file_name" "$output_file"
              fi;;
    *zip*)  number_contained_files=$(unzip -Z -1 "$output_file" | wc -l)
            # Extract archive only if it contains just one file.
            # Todo: Extract the relevant file, see e.g. Filterlists.com id 1994
            if [ "$number_contained_files" -eq 1 ]; then
              contained_filename=$(unzip -Z -1 "$output_file")
              # unzip creates a folder if it does not exist at the moment, therefore mkdir ... is not needed
              unzip "$output_file" -d $temp_folder_name
              mv "$temp_folder_name$contained_filename" "$output_file"
              rm -r "$temp_folder_name"
            fi;;
    *utf-8|*ascii*)  break;;
    *) # Anything else that is not covered (yet).
      break;;
  esac
done