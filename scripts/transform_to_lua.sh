#!/bin/bash
# This script transforms domain lists into lua modules.

# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=..
fi

# Set up variables for locations. Export them to use them inside a parallel function call.
filter_set_location=$root_dir/data_in_categories_adjusted/
final_location=$root_dir/rules3000/
mkdir -p $final_location
export filter_set_location
export final_location

# Define a function that processes one filter set. It converts a domain list into a lua module.
# Note that all domains from a filter list should be blocked.
# The merged filter set's filename must be given as $1. It is located inside $filter_set_location
transform_list() {
  # Path to filter set file
  filter_set_filename="$1"
  # Name of the filter set, i.e. filename without its suffix .domains
  filter_set_name="${1%.domains}"
  # Path to the output file
  final_destination=$final_location${filter_set_name}_domains.lua
  
  # Initialize output file
  echo "return {" > "$final_destination"
  # Add rules to output file
  while read domain; do
    echo "  \"$domain\"," >> "$final_destination"
  done < "$filter_set_location$filter_set_filename"
  # Finalize output file
  echo "}" >> "$final_destination"

  echo Domain list for "$filter_set_name" blocking was transformed into lua format.
}
export -f transform_list

# Process all domain lists in parallel using all .domains files from $filter_set_location
parallel transform_list ::: "$(ls -p $filter_set_location | grep -E "*.domains$")"
