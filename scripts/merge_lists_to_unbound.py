import os
from argparse import ArgumentParser


def parse_arguments():
    parser = ArgumentParser()
    parser.add_argument("--blocklist_file", default="blocklist.conf",
                        help="Output filename for the blocklist configuration.")
    parser.add_argument("--input_folder", default="data_in_categories_adjusted/",
                        help="Input directory containing the domainlists for each category")
    return parser.parse_args()


def generate_blocklist_conf(output_file="blocklist.conf"):
    domains = dict()
    for filename in blocklists:
        with open(os.path.join(data_folder, filename), "r") as file_in:
            tag = os.path.splitext(filename)[0]
            for domain in file_in:
                if domain.endswith("\n"):
                    domain = domain[:-1]
                if domain in domains:
                    domains[domain].add(tag)
                else:
                    domains[domain] = {tag}

    with open(output_file, "w") as file_out:
        file_out.write(f"define-tag: \"{' '.join(tags)}\"\n\n")
        for domain in domains:
            file_out.write(f"local-zone: \"{domain}\" always_nxdomain\n"
                           f"local-zone-tag: \"{domain}\" \"{' '.join(domains[domain])}\"\n")


if __name__ == '__main__':
    args = parse_arguments()
    data_folder = args.input_folder
    blocklists = sorted([x for x in os.listdir(data_folder) if x.endswith(".domains")])
    tags = [os.path.splitext(filename)[0] for filename in blocklists]
    print(f"Found tags: {' '.join(tags)}")
    print("Merging categories into one unbound configuration")
    generate_blocklist_conf(args.blocklist_file)
    print(f"Unbound configuration written to {args.blocklist_file}")
