#!/bin/bash
# This downloader requires two command line argument. Arg1=URL, Arg2=Destination for Download

# Parse arguments
url=$1
destination=$2
if [ $# -lt 2 ]
 then
   echo "$0: Not enough arguments given. Two arguments required. Arg1=URL, Arg2=Destination for Download"
   exit 1
fi

# Set timeout in seconds and number of tries for a download with wget
timeout=10
tries=3
# Define a filename for a temporary used file. The file will be downloaded to this filename and moved to
# $destination afterwards
temp_file_name=temp_file_$$  # Add current pid to temp_file name to enable runs in parallel

# Download file only if it has changed since the last download (more precise: since the last local modification) or if
# it was never downloaded before.
if [ -f "$destination" ]
  then
    # The File exists already, check its last modification.
    # Date returns a date in the following form: Mon, 11 Jan 2021 08:30:50 +0000
    # For curl the following format is needed: Mon, 11 Jan 2021 08:30:50 GMT
    last_modification_local=$(date -R -u -r "$destination")
    last_modification_local=${last_modification_local%+0000}GMT
    wget -nv --timeout=$timeout --tries=$tries --header "If-Modified-Since: $last_modification_local" -O "$temp_file_name" "$url"
    exit_code=$?
    case "$exit_code" in
      # File has changed and was downloaded again
      0)  mv "$temp_file_name" "$destination";;
      # File has not changed and wget returned with exit code 8 (Server issued an error response) and http response code 304
      8)  rm "$temp_file_name";;
      # Any other error occurred
      *)  echo -e "Error while downloading: $url\nwget finished with code $exit_code"
          rm "$temp_file_name"
          exit $exit_code;;
    esac
  else
    # File has never been downloaded before
    wget -nv --timeout=$timeout --tries=$tries -O "$destination" "$url"
    exit_code=$?
    if [ $exit_code -ne 0 ]
      then
        echo -e "Error while downloading: $url\nwget finished with code $exit_code"
        rm "$temp_file_name"
        exit $exit_code
    fi
fi