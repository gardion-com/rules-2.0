from argparse import ArgumentParser
import requests

parser = ArgumentParser()
parser.add_argument("url")
parser.add_argument("message")
args = parser.parse_args()
webhook_message = "An error occurred in `unbound-checkconf`:\n```\n{}\n```\nThe configuration for the **new architecture** was not updated " \
                  "in the current rules update.".format(args.message)
requests.post(args.url, json={"text": webhook_message})
