#!/bin/bash
# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=..
fi
# export root_dir to use it with a parallel function call
export root_dir
# Set up variables for in- and output location. Export them to use them inside a parallel function call.
meta_data_folder_categories=$root_dir/meta_data/group_info/categories/
output_location=$root_dir/data_in_categories/
mkdir -p $output_location
export meta_data_folder_categories
export output_location

# Define a function that groups filter lists to one category. Category definition filename must be given as $1. It is
# located inside $meta_data_folder_categories
group_category_domains() {
  # path to the definition/info file
  category_definition="$meta_data_folder_categories$1"
  # name of the category, i.e. filename without its suffix .txt
  category_name="${1%.txt}"
  # path to the output file
  category_destination="$output_location$category_name.domains"
  # temp file name that is used for collecting all domains and deduplication. Files moved to category_destination later.
  temp_file="${output_location}temp_file_$$"
  # add content of all files to temp_file
  while read filename; do
    # Skip comments, i.e. a line which starts with a "#","!" or "//".
    # Skip empty lines and those starting with a blank.
    case "$filename" in
    "#"* | "!"* | "//"* | " "* | "" ) continue ;;
    esac
    #echo "$root_dir/$filename"
    cat "$root_dir/$filename" >> "$temp_file"
  done <"$category_definition"
  # remove duplicates and delete temp_file
  sort -u $temp_file >"$category_destination"
  rm $temp_file
  # print success message
  echo Category "$category_name" successfully merged.
}
export -f group_category_domains

#group into categories in parallel using all .txt files from the meta_data_folder_categories
parallel group_category_domains ::: "$(ls -p $meta_data_folder_categories | grep -E "*.txt$")"
