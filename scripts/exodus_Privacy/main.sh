#!/bin/bash
# Ensure this script can be run separately from the run.sh (global main) script by setting the root_dir accordingly.
if [ -z ${root_dir+root_set} ]; then
  root_dir=../..
fi

download_script=$root_dir/scripts/download.sh
download_location=$root_dir/data_preprocessed/exodus_privacy/
download_location_raw=$root_dir/data_raw/exodus_privacy/
mkdir -p $download_location     # Directory for parsed list (contains only Domains)
mkdir -p $download_location_raw # Directory for downloaded file
# Download list file
url=https://reports.exodus-privacy.eu.org/api/trackers
destination=${download_location_raw}exodus.json
source "$download_script" "$url" "$destination"
# Load regular expressions from file
source $root_dir/scripts/regex.sh
# Extract domains from downloaded file
tracker=$(jq -r ".trackers[].network_signature" $destination)
tracker="${tracker//\\/}"
echo "$tracker" |
  grep -E -o "$regex_domain" \
    > ${download_location}exodus_privacy.domains
