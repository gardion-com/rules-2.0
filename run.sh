#!/bin/bash
# Should update all sources with the main.sh scripts in this directory and its subdirectories.

# Todo: Ensure this script is always run from the repo's root folder or set the root directory accordingly (do that automatically)
# Change working directory to root directory of this project
#cd .. todo directory is currently correct, maybe change it accordingly.
root_dir=$(pwd)

# Set start_time as output of the date command and print it in order to recognize it in the log file
start_time=$(date)
echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#
echo \# $start_time \#
echo \#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#\#

source scripts/exodus_Privacy/main.sh
echo
source scripts/sources_collected_by_hand/main.sh
echo
source scripts/lists_handmade/main.sh
echo
# Group all downloaded data into categories
source scripts/group_into_categories.sh
echo
# Involve adjustments
source scripts/adjust_filter_sets.sh
echo
# Transform rules into lua modules
source scripts/transform_to_lua.sh
echo


# Upload new filter sets to the repo.
cd rules3000
git add .
git commit -m "Rules update done $start_time"
git gc
git push
cd ..
